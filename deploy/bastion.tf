# Defining image of operating system

# Data is for retrieving information from AWS
# ami stands for Amazon Machine Images
data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  # Ensure the image we pull is from Amazon
  owners = ["amazon"]
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

  # Merge allows us to use our common (base) tags
  # and then add additional or override common
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}