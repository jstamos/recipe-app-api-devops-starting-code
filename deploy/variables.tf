# A prefix helps you determine what that resource is for when looking at the AWS Console
variable "prefix" {
  # Recipe App API Devops (RAAD)
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "jessica@jessicastamos.com"
}